﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpenCloser
{
    public class SwaggerConfiguration
    {
        /// <summary>
        /// <para>OpenCloser API v1</para>
        /// </summary>
        public const string EndpointDescription = "OpenCloser API v1";

        /// <summary>
        /// <para>/swagger/v1/swagger.json</para>
        /// </summary>
        public const string EndpointUrl = "/swagger/v1/swagger.json";

        /// <summary>
        /// <para>Juan Leal, Juan Camilo, César Rodríguez</para>
        /// </summary>
        public const string ContactName = "Juan Leal, Juan Camilo, César Rodríguez";

                /// <summary>
        /// <para>v1</para>
        /// </summary>
        public const string DocNameV1 = "v1";

        /// <summary>
        /// <para>OpenCloser API</para>
        /// </summary>
        public const string DocInfoTitle = "OpenCloser API";

        /// <summary>
        /// <para>v1</para>
        /// </summary>
        public const string DocInfoVersion = "v1";

        /// <summary>
        /// <para>Aplicación para generar Hash de cierre BlockChain</para>
        /// </summary>
        public const string DocInfoDescription = "Aplicación para generar Hash de cierre BlockChain";
    }
}
