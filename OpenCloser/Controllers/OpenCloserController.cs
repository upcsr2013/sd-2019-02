﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OpenCloser.Models;

namespace OpenCloser.Controllers
{
    [Route("api/services/app/[controller]/[action]")]
    [ApiController]
    public class OpenCloserController : Controller
    {

        /// <summary>
        /// Método que retorna el hash y el nonce generado de acuerdo a entradas
        /// </summary>x
        /// <param name="openCloser">parámetro explicado</param>
        /// <returns></returns>
        [HttpPost]
        public CerrarBloqueResponse CerrarBloque(OpenCloserModel openCloser)
        {
            long nonce = 0;
            var hash = SHA256Encrypt(string.Concat(openCloser.idBloque, nonce, openCloser.datos, openCloser.hashAnterior)); 
            while (hash.Substring(0, 3) != "000")
            {
                nonce++;
                hash = SHA256Encrypt(string.Concat(openCloser.idBloque, nonce, openCloser.datos, openCloser.hashAnterior));
            }
            var response = new CerrarBloqueResponse
            {
                Hash = hash,
                Nonce = nonce.ToString()
            };
            return response;
        }
        private string SHA256Encrypt(string input)
        {
            var provider = new SHA256CryptoServiceProvider();

            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            byte[] hashedBytes = provider.ComputeHash(inputBytes);

            var output = new StringBuilder();

            for (int i = 0; i < hashedBytes.Length; i++)
                output.Append(hashedBytes[i].ToString("x2").ToLower());

            return output.ToString();
        }
    }
}
