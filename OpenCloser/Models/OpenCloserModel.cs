﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OpenCloser.Models
{
    public class OpenCloserModel
    {
        [Required]
        public string idBloque { get; set; }
        [Required]
        public string datos { get; set; }
        [Required]
        public string hashAnterior { get; set; }
    }
}
