﻿namespace OpenCloser.Models
{
    public class CerrarBloqueResponse
    {
        public string Nonce { get; set; }
        public string Hash { get; set; }

    }
}
